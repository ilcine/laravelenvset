# Laravel Environmet Set

[![Latest Version on Packagist](https://ilcine.gitlab.io/img/packagist.svg)](https://packagist.org/packages/emr/laravel-env-set)
[![License](https://ilcine.gitlab.io/img/mit.svg)](LICENSE.md)

Change .env file variables on Laravel

_Emrullah İLÇİN_

- [Laravel Environmet Set](#laravel-environmet-set)
    - [Installation](#installation)
    - [Usage](#usage)
   
## Installation

You can install the package with [Composer](https://packagist.org/packages/emr/laravel-env-set) 

```bash
composer require emr/laravel-env-set
```

## Usage

```bash
$ php artisan emr:laravelenvset APP_NAME=Example
```

Other Examples:

```bash
$ php artisan emr:laravelenvset APP_NAME="Example 1"
$ php artisan emr:laravelenvset App_Name="Example 1"
$ php artisan emr:laravelenvset App_Name "Example 1"
$ php artisan emr:laravelenvset App_Name Example1

```



