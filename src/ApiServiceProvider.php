<?php

namespace Emr\LaravelEnvSet;

use Illuminate\Support\ServiceProvider;

use Emr\LaravelEnvSet\LaravelEnvSet;

class ApiServiceProvider extends ServiceProvider
{
	public function boot()
	{
		$this->mapCommands();  // Enable consol commands
	}

	public function register()
	{
		//
	}
	
	public function mapCommands()
	{
		if ($this->app->runningInConsole()) {
				$this->commands([
				 LaravelEnvSet::class,
				]);
		}
	}	
}
