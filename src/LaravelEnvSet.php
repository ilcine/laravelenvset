<?php

namespace Emr\LaravelEnvSet;

use Illuminate\Console\Command;
use InvalidArgumentException;

class LaravelEnvSet extends Command
{
	protected $signature = 'emr:laravelenvset {key} {value?}  {--queue=}';
 
	// Usage: emr:laravelenvset [options] [--] <key> [<value>]
	// Comment: "<key>" necessary;  "[<value>]" non-selective  
	// Ex run: "php artisan emr:envset( APP_NAME ABC | APP_NAME=Abc | APP_NAME='My Name')"
	// Help:  "php artisan help emr:laravelenvset"
	
	protected $description = 'Run the command description';

	public function __construct()
	{
			parent::__construct();
	}

	public function handle()
	{
        
        $key = $this->argument('key'); // import key from console
		$value = $this->argument('value'); // import value from console		
		
		// if non-selective [<$value>] is nothing; bat if there is $key in $value ; separate them
		if (! $value) {
		
				$parts = explode('=', $key, 2);  // convert to two array

				if (count($parts) !== 2) {
						throw new InvalidArgumentException('No value was set');  // if it not equal 2
				}

				$key = $parts[0];  // first array elements is key
				$value = $parts[1]; // second array elements is value
		}
	
		if (!preg_match('/^[a-zA-Z_]+$/', $key)) {
				throw new InvalidArgumentException('Invalid environment key. Only use letters and underscores');
		}

		// is_bool = false or thue; strpos is search ' '; and boşluk yoksa;  if found it show begin number characters of variable
		if (! is_bool(strpos($value, ' '))) {
				$value = '"' . $value . '"';  // put a quota ; line bengin and end
		}
		
		// write upercase key and default case value in the .env file
		$this->writeEnv(strtoupper($key),$value); //ok
		
	}
	
	protected function writeEnv($key,$value)
	{			
	
		$envFilePath = base_path('.env');  // path .env file ex1:
		//$envFilePath = app()->environmentFilePath();  // path .env file ex2:
		
		$contents = file_get_contents($envFilePath);  // read .env file 
		
        // Found $oldValue ; if command is NOT "==", it is a "=" variable MOVE
		if ($oldValue = $this->findOldKeyFromEnv($contents, $key)) {
			
			// Rewrite the $value in the .env file
			file_put_contents($envFilePath, str_replace(
				$key."={$oldValue}",
				$key."=$value",
				file_get_contents($envFilePath)
			));
			
			return $this->info("Environment variable with key '{$key}' has been changed from '{$oldValue}' to '{$value}'");
		}
		
		// Create new record; write new $key and $value in .env file 
		file_put_contents($envFilePath,  PHP_EOL . $key."=$value", FILE_APPEND );
		// PHP_EOL = new line, // FILE_APPEND = add under 
		
		return $this->info("A new environment variable with key '{$key}' has been set to '{$value}'");
		
	}
	
	protected function findOldKeyFromEnv(string $envFile, string $key): string
	{
		// Is there old key inside $envFile
		preg_match("/^{$key}=[^\r\n]*/m", $envFile, $matches);  

		// there is old "key+value"; it is inside $matches 
		if (count($matches)) {
			
				// old key found; and cut $key line from $matches[0]; return it
				return substr($matches[0], strlen($key) + 1);   
		}

		// old key not found
		return '';  // key spaces
	}

}
                                                                         